#!/usr/bin/python3

import pandas as pd

def main():
    flightcsv = pd.read_csv("airline_flights.csv")

    # display first 5 entries of the flightcsv dataframe
    print('========flightcsv dataframe=======\n', flightcsv.head())

    # organize data by origin and destination airport
    flightcsv_tofrom = flightcsv.groupby(['ORG_AIR', 'DEST_AIR']).size()
    print('========group by origin and destination=========\n', flightcsv_tofrom.head(20))
    
    # Display the number of flights between Huston (IAH)
    # and Atlanta (ATL) in both directions
    print("\n=====Flight from ATL to IAH and IAH to ATL=====\n")
    print(flightcsv_tofrom.loc[[("ATL", "IAH"), ("IAH", "ATL")]])
    
    
if __name__ == "__main__":
    main()    

