#!/usr/bin/python3
"""Alta3 Research - astros on ISS"""

import urllib.request
import json

MAJORTOM = "http://api.open-notify.org/astros.json"

def main():
    """reading json from api"""
    # call the api
    groundctrl = urllib.request.urlopen(MAJORTOM)

    # strip off the attachment (JSON) and read it
    # the problem here, is that it will read out as a byte
    helmet = groundctrl.read()

    # show that at this point, our data is str
    # we want to convert this to list / dict
    print('helmet from api', helmet)

    helmetson = json.loads(helmet.decode("utf-8"))

    # this should say bytes
    print('type of helmet', type(helmet))

    # this should say dict
    print('type of helmet in json', type(helmetson))

    print('number: ', helmetson["number"])

    # this returns a LIST of the people on this ISS
    print('people', helmetson["people"])

    # list the FIRST astro in the list
    print('first astro', helmetson["people"][0])

    # list the SECOND astro in the list
    print('second astro', helmetson["people"][1])

    # list the LAST astro in the list
    print('last astro', helmetson["people"][-1])

    # display every item in a list
    print("\nall astros:\n")
    for astro in helmetson["people"]:
        # display what astro is
        print(astro)

    print("\nall astros names:\n")
    # display every item in a list
    for astro in helmetson["people"]:
        # display ONLY the name value associated with astro
        print(astro["name"])

if __name__ == "__main__":
    main()

