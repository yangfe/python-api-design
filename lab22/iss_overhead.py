#!/usr/bin/python3
"""Using an HTTP GET to determine when the ISS will pass over head"""

import requests
import time

# python3 -m pip install requests
import requests

def main():
    while True:
        try:
            lat = input('\nenter your latitude: ')
            lon = input('enter your longitude: ')

            url = f"http://api.open-notify.org/iss-pass.json?lat={lat}&lon={lon}"
            resp = requests.get(url).json()

            if resp.status_code != requests.codes.ok:
                print(f"Non-200 response code; try again")
                continue

            if resp['message'] != 'success':
                print(f"try again; {resp['reason']}")
                continue

            passtimes = resp['response']
            for t in passtimes:
                print(f"ISS passes over head at this time: {time.ctime(t['risetime'])} for {t['duration']}s")
        except KeyboardInterrupt:
            if input('\n type exit to exit: ') != 'exit':
                continue
            break


if __name__ == "__main__":
    main()

