import requests
import time

def main():
    date_url = 'http://date.jsontest.com'
    r = requests.get(date_url).json()
    my_time = time.ctime(r['milliseconds_since_epoch'])

    ip_url = 'http://ip.jsontest.com'
    r = requests.get(ip_url).json()
    my_ip = r['ip']

    my_servers = []
    with open('myservers.txt') as f:
        for line in f:
            my_servers.append(line.strip())

    # Jason Edited - turned value into str
    payload = {'json': """ {
        'time': my_time,
        'ip': my_ip,
        'mysvrs': my_servers
        }"""
    }


    print("my payload", payload)

    validate_url = 'http://validate.jsontest.com'
    resp = requests.post(validate_url, data=payload)

    print(resp.text)
    #print(f"is your JSON valid? {resp['validate']}")
    
if __name__ == "__main__":
    main()
    

        
    

