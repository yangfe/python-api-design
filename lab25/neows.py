#!/usr/bin/python3
import requests
import os
from dotenv import dotenv_values

## Define NEOW URL
NEOURL = "https://api.nasa.gov/neo/rest/v1/feed?"

# this function grabs our credentials
# it is easily recycled from our previous script
def returncreds():
    ## first I want to grab my credentials
    with open("/home/student/nasa.creds", "r") as mycreds:
        nasacreds = mycreds.read()
    ## remove any newline characters from the api_key
    nasacreds = "api_key=" + nasacreds.strip("\n")
    return nasacreds

def returncreds_env():
    config = {
        **dotenv_values(".env"),  # load shared development variables
        **os.environ,  # override loaded values with environment variables
    }
    return f"api_key={config['API_KEY']}"

# this is our main function
def main():
    ## first grab credentials
    #nasacreds = returncreds()
    nasacreds = returncreds_env()

    ## update the date below, if you like
    user_input_start_date = input('enter start date (YYYY-MM-DD): ')
    startdate = f"start_date={user_input_start_date}"

    ## the value below is not being used in this
    ## version of the script
    user_input_end_date = input('enter end date (YYYY-MM-DD): ')
    enddate = f"end_date={user_input_end_date}"

    # make a request with the request library
    neowrequest = requests.get(NEOURL + startdate + "&" + enddate + "&" + nasacreds)

    # strip off json attachment from our response
    neodata = neowrequest.json()

    ## display NASAs NEOW data
    print(neodata)

if __name__ == "__main__":
    main()

