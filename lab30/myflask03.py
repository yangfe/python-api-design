#!/user/bin/python3

from flask import Flask, redirect, url_for, request, render_template

app = Flask(__name__)

@app.route("/")
@app.route("/start")
def start():
    return render_template("postmaker.html")


@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        user = "defaultuser"
        if request.form.get("nm"):
            user = request.form.get("name")
    return redirect(url_for("success", name = user))


@app.route("/success/<name>")
def success(name):
    return f"Welcome {name}\n"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224)
