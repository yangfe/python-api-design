#!/usr/bin/env python3
from flask import Flask, render_template, request
app = Flask(__name__)

@app.route("/<username>")
def index(username):
    return render_template("hellobasic.html", name = username)


@app.route("/guess/<int:number>")
def guess(number):
    return render_template("guess.html", num = number)


@app.route("/ciscoios/")
def ciscoios():
    try:
        qparams = {}
        # user passes switchname= or default "bootstrapped switch"
        qparams["switchname"]  = request.args.get("switchname", "bootstrapped switch")
        # user passes username= or default "admin"
        qparams["username"]  = request.args.get("username", "admin")
        # user passes gateway= or default "0.0.0.0"
        qparams["defaultgateway"] = request.args.get("gateway", "0.0.0.0")
        # user passes ip= or default "0.0.0.0"
        qparams["switchIP"] = request.args.get("ip", "0.0.0.0")
        # user passes mask= or default "255.255.255.0"
        qparams["netmask"] = request.args.get("mask", "255.255.255.0")
        # user passes mtu= or default "1450"
        qparams["mtusize"] = request.args.get("mtu", "1450")

        return render_template("baseIOS.conf.j2", **qparams)

    except Exception as err:
        return err


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224)
