#!/usr/bin/env python3
from flask import Flask
from flask import make_response
from flask import request
from flask import render_template
from flask import redirect
from flask import url_for

app = Flask(__name__)

# entry point for our users
# renders a template that asks for their name
# login.html points to /setcookie
@app.route("/login")
@app.route("/")
def index():
    return render_template("login.html")

# set the cookie and send it back to the user
@app.route("/setcookie", methods = ["POST", "GET"])
def setcookie():
    if request.method == "POST":
        user = request.form.get("nm", "defaultuser")

        resp = make_response(render_template("readcookie.html"))
        resp.set_cookie("userID", user)
        return resp

    if request.method == "GET":
        return redirect(url_for("index"))

# check users cookie for their name
@app.route("/getcookie")
def getcookie():
    name = request.cookies.get("userID", "defaultuser")
    return f"<h1>Welcome {name}</h1>"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224)


