#!/usr/bin/python3

from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey

engine = create_engine('sqlite:///:memory', echo=True)

metadata_obj = MetaData()
users = Table('users', metadata_obj,
         Column('id', Integer, primary_key=True),
         Column('name', String),
         Column('fullname', String),
     )

addresses = Table('addresses', metadata_obj,
   Column('id', Integer, primary_key=True),
   Column('user_id', None, ForeignKey('users.id')),
   Column('email_address', String, nullable=False)
)

metadata_obj.create_all(engine)
